package com.akamahesh.gifninza;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Mahesh Bhatt.
 */


public class BitmapAdapter extends RecyclerView.Adapter<BitmapAdapter.ViewHolder> {

    private Context context;
    private List<Bitmap> bitmapList;

    public BitmapAdapter(Context context, List<Bitmap> bitmapList) {
        this.context = context;
        this.bitmapList = bitmapList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(context).inflate(R.layout.row_image_layout, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Bitmap bitmap = bitmapList.get(position);
        if(bitmap!=null){
            int size = bitmap.getByteCount();
            Glide.with(context)
                    .load(bitmap)
                    .into(holder.imageView);
        }


    }

    @Override
    public int getItemCount() {
        return bitmapList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_frame);
        }
    }
}
