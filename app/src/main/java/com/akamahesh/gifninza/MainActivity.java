package com.akamahesh.gifninza;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.FFmpegExecuteResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ImageView ivGif;
    private List<Bitmap> bitmapList;
    private BitmapAdapter bitmapAdapter;
    private final static String TAG = MainActivity.class.getSimpleName();
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PDFBoxResourceLoader.init(getApplicationContext());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progress_bar);
        setSupportActionBar(toolbar);
        ivGif = findViewById(R.id.iv_gif);
        Glide.with(this)
                .load(R.raw.splash_before)
                .into(ivGif);


        InputStream inputStream = getResources().openRawResource(R.raw.android_gifi);



        bitmapList = new ArrayList<>();
        bitmapAdapter = new BitmapAdapter(this, bitmapList);
        RecyclerView recyclerView = findViewById(R.id.rv_bitmaps);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(bitmapAdapter);

        FloatingActionButton fab1 = findViewById(R.id.fab_1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissions();

            }
        });

        FloatingActionButton fab2 = findViewById(R.id.fab_2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LongOperation2().execute();
            }
        });
    }

    String gifFilePath;
    String gifVideoPath;

    private void warmup() {
        File dir = new File (Environment.getExternalStorageDirectory() , "/XXX");
        if(!dir.exists()){
            dir.mkdirs();
        }
        File textFile = new File(dir.getAbsolutePath(),"readme.txt");
        boolean created = textFile.exists();
        Log.d(TAG, "warmup: ");

        File gifFile = new File(Environment.getExternalStorageDirectory() + "/gifNinza/android_gifi.gif");
        boolean exi = gifFile.exists();
        if(gifFile.exists()){
            gifFilePath = gifFile.getAbsolutePath();
        }
        File gifVideo = new File(Environment.getExternalStorageDirectory()+"/gifNinza/giphy_video.mp4");
        if(gifVideo.exists()){
            gifVideoPath = gifVideo.getAbsolutePath();
        }else{
            gifVideoPath = Environment.getExternalStorageDirectory()+"/gifNinza/images/image-%3d.png";
        }


    }

    private void checkPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        101);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    private void loadBinary() {
        FFmpeg fFmpeg = FFmpeg.getInstance(this);
        try {
            fFmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    super.onFailure();
                }

                @Override
                public void onSuccess() {
                    super.onSuccess();
                }

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    makeVideo();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeVideo() {
        String[] conversionCommand = {"-i",gifFilePath,"-r","5","-f","image2",gifVideoPath};
        try {
            FFmpeg.getInstance(this).execute(conversionCommand, fFmpegExecuteResponseHandler);
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }


    FFmpegExecuteResponseHandler fFmpegExecuteResponseHandler = new FFmpegExecuteResponseHandler() {
        @Override
        public void onSuccess(String message) {
            Log.d(TAG, "onSuccess");
            Toast.makeText(MainActivity.this,"success "+message,Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onProgress(String message) {
            Log.d(TAG, "onProgress");

        }

        @Override
        public void onFailure(String message) {
            Log.d(TAG, "onFailure "+message);
            Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStart() {
            Log.d(TAG, "onStart");
            progressBar.setVisibility(View.VISIBLE);

        }

        @Override
        public void onFinish() {
            Log.d(TAG, "onFinish");
            Toast.makeText(MainActivity.this,"onFinish ",Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
        }
    };


    private class LongOperation2 extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Log.d(TAG, "onPreExecute: ");

        }

        @Override
        protected String doInBackground(String... gifDecoders) {

            warmup();
            loadBinary();


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            bitmapAdapter.notifyDataSetChanged();
        }

        private void sleep(int millisecond) {
            try {
                Thread.sleep(millisecond);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:

                return true;

            case R.id.action_color_palette:
                startActivity(new Intent(this,ColorPaletteActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
