package com.akamahesh.gifninza;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.widget.SeekBar;

/**
 * Created by Mahesh Bhatt.
 */


public class ColorPaletteSeekBar extends android.support.v7.widget.AppCompatSeekBar implements SeekBar.OnSeekBarChangeListener {


    private OnColorChangeListener mOnColorChangeListener;
    private int mColor;
    private boolean isFirst = false;
    private boolean isSecond = false;
    private int firstProgress=0;
    int r, g, b;

    public void setOnColorChangeListener(OnColorChangeListener onColorChangeListener) {
        this.mOnColorChangeListener = onColorChangeListener;
    }


    public ColorPaletteSeekBar(Context context) {
        super(context);
        setOnSeekBarChangeListener(this);
        setMinimumHeight(0);
    }

    public ColorPaletteSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnSeekBarChangeListener(this);
        setMinimumHeight(0);
    }

    public ColorPaletteSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnSeekBarChangeListener(this);
        setMinimumHeight(0);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        init();
    }

    /**
     * Initialize the color seekbar with the gradient
     **/
    public void init() {
        isSecond = false;
        isFirst = false;
        int[] gradient = new int[]{0xFF000000, 0xFF0000FF, 0xFF00FF00, 0xFF00FFFF, 0xFFFF0000, 0xFFFF00FF, 0xFFFFFF00, 0xFFFFFFFF};
        LinearGradient colorGradient = new LinearGradient(
                0.f, //the x coordinate for the start of the gradient line
                0.f, //the y coordinate for the start of the gradient line
                this.getMeasuredWidth() - this.getThumb().getIntrinsicWidth(), //the x-coordinate for the end of the gradient line
                0.f, //the y coordinates for the end of gradient line
                gradient, //the colors to be distributed along the gradient line
                null,// the relative position[0..1] of each corresponding color in the colors array. If this is null, the colors are distributed evenly along the gradient line.
                Shader.TileMode.CLAMP);//The shader tiling mode.
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
        shapeDrawable.getPaint().setShader(colorGradient);
        this.setProgressDrawable(shapeDrawable);
        this.setMax(256 * 7 - 1);
        this.setProgress(firstProgress);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (null == mOnColorChangeListener) {
            return;
        }
        if (isFirst) {
            r = 0;
            g = 0;
            b = 0;

            if (progress < 256) {
                b = progress;
            } else if (progress < 256 * 2) {
                g = progress % 256;
                b = 256 - progress % 256;

            } else if (progress < 256 * 3) {
                g = 255;
                b = progress % 256;
            } else if (progress < 256 * 4) {
                r = progress % 256;
                g = 256 - progress % 256;
                b = 256 - progress % 256;
            } else if (progress < 256 * 5) {
                r = 255;
                g = 0;
                b = progress % 256;
            } else if (progress < 256 * 6) {
                r = 255;
                g = progress % 256;
                b = 256 - progress % 256;
            } else if (progress < 256 * 7) {
                r = 255;
                g = 255;
                b = progress % 256;
            }
            mColor = Color.argb(255, r, g, b);
            mOnColorChangeListener.changeTextColor(seekBar, mColor);
            firstProgress = progress;
            isFirst = false;
        } else if(isSecond){
            mColor = Color.argb(progress, r, g, b);
            mOnColorChangeListener.changeTextColor(seekBar, mColor);
        }else{
            isFirst=true;
        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        if (mOnColorChangeListener == null) {
            return;
        }
       // mOnColorChangeListener.onStartTrackingTouch(seekBar);

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (null == mOnColorChangeListener) {
            return;
        }

    }

    public void changePallete(int[] gradient) {
        LinearGradient colorGradient = new LinearGradient(
                0.f, //the x coordinate for the start of the gradient line
                0.f, //the y coordinate for the start of the gradient line
                this.getMeasuredWidth() - this.getThumb().getIntrinsicWidth(), //the x-coordinate for the end of the gradient line
                0.f, //the y coordinates for the end of gradient line
                gradient, //the colors to be distributed along the gradient line
                null,// the relative position[0..1] of each corresponding color in the colors array. If this is null, the colors are distributed evenly along the gradient line.
                Shader.TileMode.CLAMP);//The shader tiling mode.
        ShapeDrawable shapeDrawable = new ShapeDrawable(new RectShape());
        shapeDrawable.getPaint().setShader(colorGradient);
        this.setProgressDrawable(shapeDrawable);
        this.setMax(256 * 7 - 1);
        isSecond = true;
    }


    public void setBrightness() {
        isFirst = false;
        isSecond = true;
        changePallete(new int[]{0xFFFFFF, mColor});
        this.setMax(255);
    }

    /**
     * A callback that notifies clients when the color has been changed.
     * This includes changes that were initiated by the user through a
     * touch gesture or arrow key /trackball as well as chages that were initiated programatically.
     */
    public interface OnColorChangeListener {
        /**
         * Notification that color has changed
         *
         * @param seekBar
         * @param color
         * @param b
         */
       // void onColorChanged(SeekBar seekBar, int color, boolean b);

        /**
         * Notificaiton that the user has startd a touch gesture.
         * Client may want to use this to disable advancing the seekbar.
         *
         * @param seekBar
         */
       // void onStartTrackingTouch(SeekBar seekBar);

        /**
         * Notificaiton that the user has finished a touch gesture.
         * Clients may want to use this to re-enable advancing the seekbar.
         *
         * @param seekBar
         * @param mColor
         */
       // void onStopTrackingTouch(SeekBar seekBar, int mColor);

        void changeTextColor(SeekBar seekBar, int mColor);
    }


}
