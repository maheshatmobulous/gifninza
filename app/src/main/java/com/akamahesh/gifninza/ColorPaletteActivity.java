package com.akamahesh.gifninza;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

public class ColorPaletteActivity extends AppCompatActivity {

    private ColorPaletteSeekBar colorPaletteSeekBar;
    private TextView tvText;
    private ImageView ivColorize;
    private ImageView ivBrightness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_palette);
        colorPaletteSeekBar = findViewById(R.id.seekbar_palette);
        colorPaletteSeekBar.setOnColorChangeListener(onColorChangeListener);
        tvText = findViewById(R.id.tv_text);
        ivBrightness = findViewById(R.id.iv_brightness);
        ivColorize = findViewById(R.id.iv_colorize);
        ivColorize.setSelected(true);
        tvText.setTextColor(getResources().getColor(R.color.colorBlack));



        ivColorize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ivColorize.isSelected()){
                    ivColorize.setColorFilter(getResources().getColor(R.color.colorAccent));
                    ivBrightness.setColorFilter(getResources().getColor(R.color.colorBlack));
                    ivColorize.setSelected(true);
                    ivBrightness.setSelected(false);
                    colorPaletteSeekBar.init();
                }

            }
        });

        ivBrightness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ivBrightness.isSelected()){
                    ivBrightness.setColorFilter(getResources().getColor(R.color.colorAccent));
                    ivColorize.setColorFilter(getResources().getColor(R.color.colorBlack));

                    ivBrightness.setSelected(true);
                    ivColorize.setSelected(false);
                    colorPaletteSeekBar.setBrightness();
                }
            }
        });

    }

    ColorPaletteSeekBar.OnColorChangeListener onColorChangeListener = new ColorPaletteSeekBar.OnColorChangeListener() {

        @Override
        public void changeTextColor(SeekBar seekBar, int color) {
            tvText.setTextColor(color);
        }
    };



}
